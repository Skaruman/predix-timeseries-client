package com.example.websocket.client.demo;

/**
 * Created by skarumanchi on 08/06/2017.
 */
public class DataPointVTQ {
    public Double value;
    public Long capsules;
    public Long timeStamp;
    public int feeder;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Long getCapsules() {
        return capsules;
    }

    public void setCapsules(Long capsules) {
        this.capsules = capsules;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getFeeder() {
        return feeder;
    }

    public void setFeeder(int feeder) {
        this.feeder = feeder;
    }
}
