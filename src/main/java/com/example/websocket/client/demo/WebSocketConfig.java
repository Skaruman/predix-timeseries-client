package com.example.websocket.client.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.*;

/**
 * Created by skarumanchi on 08/06/2017.
 */
@Configuration
@EnableWebSocket
@EnableScheduling
public class WebSocketConfig {
@Bean
    public DataGenerator getDataGenerator(){
    return new DataGeneratorMock();
}
@Bean
    public TimeSeries getTimeSeries(){
        return new TimeSeries();
}
    @Bean
    public IOauthRestConfig defaultOauthRestConfig() {
        return new DefaultOauthRestConfig();
    }

    @Bean
    public IWebSocketConfig defaultWebSocketConfig() {
        return new DefaultWebSocketConfigForTimeseries();
    }

    @Bean
    public ITimeseriesConfig defaultTimeseriesConfig() {
        return new DefaultTimeseriesConfig();
    }

    @Bean
    public JsonMapper jsonMapper() {
        return new JsonMapper();
    }

    @Bean
    public RestClient restClient() {
        return new RestClientImpl();
    }

    @Bean
    public WebSocketClient webSocketClient() {
        return new WebSocketClientImpl();
    }

    @Bean
    public TimeseriesClient timeseriesClient() {
        return new WSTimeseriesWrapper();
    }
}
