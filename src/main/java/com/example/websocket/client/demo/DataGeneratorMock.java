package com.example.websocket.client.demo;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;
import java.util.Date;

/**
 * Created by skarumanchi on 08/06/2017.
 */

public class DataGeneratorMock implements DataGenerator {
    static final Logger log = LoggerFactory.getLogger(DataGeneratorMock.class);
    @Autowired
    private TimeSeries timeSeries;
    @Override
    @Scheduled(fixedDelay = 1000)
    public DataPointVTQ sendDataPoint() throws IOException, JSONException {
        DataPointVTQ dataPointVTQ = new DataPointVTQ();
        dataPointVTQ.feeder=1;
        dataPointVTQ.timeStamp= new Date().getTime();
        dataPointVTQ.capsules=1L;
        dataPointVTQ.value=2.2;
        log.info("Data GeneratorMock");
        //send to timeseries
        timeSeries.sendTimeSeriesData(dataPointVTQ);
        return dataPointVTQ;
    }


}
