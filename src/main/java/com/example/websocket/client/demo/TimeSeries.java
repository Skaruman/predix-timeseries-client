package com.example.websocket.client.demo;

import com.ge.predix.solsvc.restclient.impl.RestClient;
import com.ge.predix.timeseries.client.ClientFactory;
import com.ge.predix.timeseries.client.TenantContext;
import com.ge.predix.timeseries.client.TenantContextFactory;
import com.ge.predix.timeseries.exceptions.PredixTimeSeriesException;
import com.ge.predix.timeseries.model.builder.IngestionRequestBuilder;
import com.ge.predix.timeseries.model.builder.IngestionTag;
import com.ge.predix.timeseries.model.datapoints.DataPoint;
import com.ge.predix.timeseries.model.datapoints.Quality;
import com.ge.predix.timeseries.model.response.IngestionResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created by skarumanchi on 06/06/2017.
 */

public class TimeSeries {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TimeSeries.class);

    @Value("predix.timeseries.ingestUri")
    String ingestionUri;
    @Value("predix.timeseries.zone-http-header-name")
    String predixZoneIdHeaderName;
    @Value("predix.timeseries.zoneid")
    String predixZoneId;

    private String authToken;
    @Autowired
    private RestClient rc;



    public void sendTimeSeriesData(Long capsules, int feeder, long timeStamp) throws ParseException, IOException, JSONException{

        TenantContext tenant = null;
        try {
            authToken = getAuthToken();
            tenant = TenantContextFactory.createIngestionTenantContextFromProvidedProperties
                    (ingestionUri, authToken, predixZoneIdHeaderName, predixZoneId);


            IngestionRequestBuilder ingestionBuilder = IngestionRequestBuilder.createIngestionRequest()
                    .withMessageId(Long.toString(timeStamp))
                    .addIngestionTag(IngestionTag.Builder.createIngestionTag()
                            .withTagName("Counts")
                            .addDataPoints(
                                    Arrays.asList(
                                            new DataPoint(timeStamp, capsules, Quality.GOOD)
                                    )
                            )
                            .addAttribute("SensorID", Integer.toString(feeder))
                            .addAttribute("Location", "Lobby")
                            .build());

            String json = null;
            json = ingestionBuilder.build().get(0);

            IngestionResponse response = null;
            response = ClientFactory.ingestionClientForTenant(tenant).ingest(json);
            String responseStr = response.getMessageId() + response.getStatusCode();
        } catch (PredixTimeSeriesException e) {
            logger.error("PredixTimeSeriesException: " + e.getLocalizedMessage());
        } catch (URISyntaxException e) {
            logger.error("URISyntaxException: " + e.getLocalizedMessage());
        } catch (IOException e) {
            logger.error("IOException: " + e.getLocalizedMessage());
        }

    }

    private String getToken() {

        return null;
    }


    public TimeSeries()  {
        logger.info("New Time Seris Instance created");
    }

    public void sendTimeSeriesData(DataPointVTQ dataPointVTQ) throws IOException, JSONException {
        sendTimeSeriesData(dataPointVTQ.getCapsules(),dataPointVTQ.getFeeder(),dataPointVTQ.getTimeStamp());
    }
    private String getAuthToken()throws ParseException, IOException, JSONException {
        String token =null;
       try {
           //generates token so you can call the assets
           HttpResponse httpResponse = rc.post(
                   "https://1e8cc081-08ba-4adc-a081-506e6fa1c26a.predix-uaa.run.aws-usw02-pr.ice.predix.io/oauth/token"
                   , "grant_type=client_credentials", rc.addZoneToHeaders(rc.getOauthHttpHeaders(), "1e8cc081-08ba-4adc-a081-506e6fa1c26a"));
           HttpEntity responseEntity = httpResponse.getEntity();
           String tokenResponse = EntityUtils.toString(responseEntity);
           JSONObject jo = new JSONObject(tokenResponse);
           token = jo.getString("access_token");
       }catch(Exception e){
           logger.info(e.getLocalizedMessage());
           e.printStackTrace();
       }
    return token;
}


}
