package com.example.websocket.client.demo;

import org.json.JSONException;

import java.io.IOException;

/**
 * Created by skarumanchi on 08/06/2017.
 */
public interface DataGenerator {

    public DataPointVTQ sendDataPoint() throws IOException, JSONException;

}

